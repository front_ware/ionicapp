(function () {
	'use strict';

	angular
		.module('App')
		.controller('HomeController', HomeController);

	HomeController.$inject = ['$scope', '$ionicPopup'];
	function HomeController($scope, $ionicPopup) {

		// $scope.users = [];

		$scope.HelloWorld = function () {
			$ionicPopup.alert({
				title: 'Hello World',
				template: '<p class="center">This is a simple popup message</p>',
     			cssClass: 'animated bounceInDown'
			});
		};

	}
})();