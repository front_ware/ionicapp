(function () {
    'use strict';

    angular
        .module('App')
        .controller('PlayerController', PlayerController);

    PlayerController.$inject = ['$scope','$stateParams'];

    function PlayerController($scope,$stateParams) {
        var mp3 = new Audio('../../mp3/dixie-horn_daniel-simion.mp3');

        $scope.color = $stateParams.color;

        $scope.soundPlay = function() {
            mp3.play();
        }
    }

})();