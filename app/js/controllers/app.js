(function () {
    'use strict';

    angular
        .module('App')
        .controller('AppController', AppController);

    AppController.$inject = ['$scope'];
    function AppController($scope) {



        $scope.items = [
            {
                color: "#3DBEC9",
                icon: "ion-ios-time",
                title: "Chronometer",
                link: "app.chrono"
            },
            {
                color: "#E47500",
                icon: "ion-play",
                title: "Player",
                link: "app.player"
            },
            {
                color: "#7e7e7e",
                icon: "ion-android-hand",
                title: "Accelerometr",
                link: "app.acc"
            }
        ];

        $scope.exitApp = function () {
            ionic.Platform.exitApp();
        };

    }
})();