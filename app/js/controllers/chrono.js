(function () {
    'use strict';

    angular
        .module('App')
        .controller('ChronoController', ChronoController);

    ChronoController.$inject = ['$scope','$stateParams'];

    function ChronoController($scope,$stateParams) {
        var chrono = new Chronometer();
        var labels = {
            start: 'Start Time',
            stop: 'Stop Time'
        };

        $scope.color = $stateParams.color;
        $scope.currentTime = 0;
        $scope.buttonLabel = labels.start;

        $scope.toggleTime = function () {
            if($scope.buttonLabel == labels.start) {
                chrono.startCounter();
                $scope.buttonLabel = labels.stop;
            } else {
                chrono.stopCounter();
                $scope.buttonLabel = labels.start;
            }
        };

        function Chronometer() {
            console.log('chronometer initialized');
        }

        Chronometer.prototype.startCounter = function () {
            this.start = new Date();

            this.interval = setInterval(this.updateTime.bind(this), 10);
        };

        Chronometer.prototype.stopCounter = function () {
            clearInterval(this.interval);
        };

        Chronometer.prototype.updateTime = function () {
            $scope.currentTime = new Date() - this.start;
            $scope.$apply();
        };
    }

})();